import React from 'react';

import { pm } from '../../../../turtleos-pm/pm';

const DURATION = 5000;

export function Notification({data}) {

    const cta = pm.getState().cta;

    const [shown, setShown] = React.useState(false);
    const [hovered, setHovered] = React.useState(false);

    React.useEffect(()=>{
        setTimeout(()=>setShown(true), 300);
        setTimeout(()=>setShown(false), DURATION);
        setTimeout(()=>cta.removeNotification(data.id), DURATION+700)
    }, [])

    return (
         <div style={{
             display:'flex',
             flexDirection: 'column',
             flexWrap: 'wrap',
             minHeight: '5rem',
             alignItems: 'start',
             justifyContent: 'center',
             right: 0,
             transform: `translateX(${shown?'-0.8rem':'120%'})`,
             position:'absolute',
             color: '#efefef',
             transition: 'transform .4s, opacity .2s',
             top: '0.8rem',
             padding: '0.4rem',
             paddingLeft: '3rem',
             paddingRight: '0.8rem',
             borderRadius: '8px',
             background: '#0e0e0e',
             opacity: hovered?1:0.85
         }}
              key={data.id}
              onClick={data.onClick}
              onMouseEnter={()=>setHovered(true)}
              onMouseLeave={()=>setHovered(false)}>
      <div style={{
        height: '100%',
        position:'absolute',
        left:'0.5rem',
        width:'2rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}>{data.icon}</div>
      <b style={{
        width: 'max-content'
      }}>{data.title}</b>
      <p style={{
        width:'max-content',
        margin:0
      }}>{data.content}</p>
    </div>
    );
}
